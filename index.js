
function vendingMachine (email, money, drinkChoice = "coffee") {
    var msg = ''; let checkEmail; let checkMoney;
    
    if (email) {
        if(typeof email != 'string') {
            msg = "Invalid email input";
        } else {
            checkEmail = true;
        }
    } else {
        msg = "Sorry can't process it until your email filled";
    }

    if (money) {
        if(typeof money != 'number') {
            msg = "Invalid money input";
        } else {
            checkMoney = true;
        }
    } else {
        msg = "Sorry can't process it until you put your money";
    }

    let harga = 0;

    switch (drinkChoice) {
        case 'mineral water':
            harga = 5000;
            break; 
        case 'cola':
            harga = 7500;
            break;
        case 'coffee':
            harga = 12250;
            break;
    }

    if(checkEmail && checkMoney) {
        if (money >= harga){
            let kembalian = money - harga;
    
            msg = `Welcome ${email} to May's Vending Machine\n`;
            msg += `Your choice is ${drinkChoice}, here's your drink\n`;
            msg += `Your changes are ${kembalian}\n`;
            msg += `Thank you`;
        } else {
            let kekurangan = harga - money;
    
            msg = `Welcome ${email} to May's Vending Machine\n`;
            msg += `Sorry, insufficient balance we can't process your ${drinkChoice}\n`;
            msg += `You need ${kekurangan} more to buy this drink\n`;
            msg += `Thank you`;
        }
    } else if (!email && !money) { // empty email && money
        msg = "Please check your input";
    }

    return msg;
}

console.log("Case 1\n" + vendingMachine('', 30000)); //case 1, output: Sorry can't process it until your email filled
console.log("Case 2\n" + vendingMachine(true, 30000)); //case 2, output: Invalid input
console.log("Case 3\n" + vendingMachine('g2academy@mail.com', 0)); //case 3, output: Sorry can't process it until you put your money
console.log("Case 4\n" + vendingMachine('g2academy@mail.com', "10000")); //case 4, output: Invalid input
console.log("Case 5\n" + vendingMachine('g2academy@mail.com', 20000)); //case 5, output: as expected
console.log("Case 6\n" + vendingMachine('g2academy@mail.com', 5000, 'cola')); //case 6, output: as expected